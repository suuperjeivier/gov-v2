package com.solucionesdigitales.vote.repository.orderday;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.solucionesdigitales.vote.entity.orderday.ParagraphOD;



public interface  ParagraphODRepository extends MongoRepository<ParagraphOD, String> {


}
